var sessionUtils = require('../utils/sessionUtils');
var util = require("util");
var databaseUtils = require('../utils/databaseUtils');

module.exports = {
    showHomePage: function* (next) {
        yield this.render('login',{

        });
    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/app/login');
    },
    showLoginPage:function*(next){
        yield this.render("signup",{

        });
    },
    signup:function*(next){
        var email = this.request.body.email;
        var pwd = this.request.body.pwd;
        var res = yield databaseUtils.executeQuery(util.format('insert into user(email,pwd) values("%s","%s")',email,pwd));
        this.redirect("/app/login");
    },
    login:function*(next){
        var email = this.request.body.email;
        var pwd = this.request.body.pwd;
        var user = yield databaseUtils.executeQuery(util.format('select * from user where email="%s" and pwd="%s"',email,pwd));
        if(user.length == 0){
            yield this.render("login",{
                msg:'Invalid User'
            })
        }else{
            sessionUtils.saveUserInSession(user[0],this.cookies);
            this.redirect('/app/home');
        }
    },
    showDash:function*(next){
        yield this.render('home');
    }
}
