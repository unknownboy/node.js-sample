var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    router.get('/login', welcomeCtrl.showHomePage);
    router.get("/signup",welcomeCtrl.showLoginPage);
    router.post('/signup',welcomeCtrl.signup);
    router.post('/login',welcomeCtrl.login);
    router.get('/home',welcomeCtrl.showDash);
    router.get('/logout',welcomeCtrl.logout);

    return router.middleware();
}
